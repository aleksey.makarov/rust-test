# I am learning Rust

> I am learning Rust

## Books

- Some Rust Book (how to implement list): https://rust-unofficial.github.io/too-many-lists/index.html
- Compiler development guide: https://rustc-dev-guide.rust-lang.org/
- Rust Atomics and Locks: Low-Level Concurrency in Practice: https://marabos.nl/atomics/
- Asynchronous Programming in Rust: https://rust-lang.github.io/async-book/

## Articles

- Error handling: https://www.reddit.com/r/rust/comments/uhw74l/errorhandling/
- Gtk with Rust:
    - https://gtk-rs.org/
    - https://gtk-rs.org/gtk4-rs/stable/latest/book/introduction.html
- Closures: https://hashrust.com/blog/a-guide-to-closures-in-rust/
- Rust/C:
    - https://developers.redhat.com/articles/2022/09/01/3-essentials-writing-linux-system-library-rust
    - https://www.greyblake.com/blog/exposing-rust-library-to-c/

