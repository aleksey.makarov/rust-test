use std::cell::Cell;

use gtk4::prelude::*;
use gtk4::{glib, Application, ApplicationWindow, Orientation};
use gtk4::{Button, Box, Label};

const APP_ID: &str = "org.gtk_rs.HelloWorld2";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(application: &Application) {

    let number = Cell::new(0);

    let label = Label::builder()
        .label(format!("{}", number.get()))
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    let button_increase = Button::builder()
        .label(format!("+"))
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    let button_decrease = Button::builder()
        .label(format!("-"))
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    let gtk_box = Box::builder()
        .orientation(Orientation::Vertical)
        .build();

    gtk_box.append(&label);
    gtk_box.append(&button_increase);
    gtk_box.append(&button_decrease);

    button_increase.connect_clicked(move |me| {
        let new = number.get() + 1;
        number.set(new);
        let l = format!("Increase {}", new);
        me.set_label(&l);
    });

    let window = ApplicationWindow::builder()
        .application(application)
        .child(&gtk_box)
        .title("My GTK App")
        .build();

    window.present();
}
