{
  description = "Build a cargo project";

  nixConfig.bash-prompt = "[\\033[1;36mdevelop \\033[1;33mrust\\033[0m]$ ";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils.follows = "flake-utils";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-analyzer-src.follows = "";
    };

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };

    nix-vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
      inputs.flake-compat.follows = "flake-compat";
    };
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    fenix,
    flake-utils,
    advisory-db,
    nix-vscode-extensions,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};

      extensions = nix-vscode-extensions.extensions.${system};

      inherit (pkgs) lib vscode-with-extensions vscodium;

      vscode = vscode-with-extensions.override {
        vscode = vscodium;
        vscodeExtensions = [extensions.open-vsx-release.rust-lang.rust-analyzer];
      };

      craneLib = crane.lib.${system};
      src = craneLib.cleanCargoSource (craneLib.path ./.);

      # Common arguments can be set here to avoid repeating them later
      commonArgs = {
        inherit src;

        buildInputs =
          [
            # Add additional build inputs here
          ]
          ++ lib.optionals pkgs.stdenv.isDarwin [
            # Additional darwin specific inputs can be set here
            pkgs.libiconv
          ];

        # Additional environment variables can be set directly
        # MY_CUSTOM_VAR = "some value";
      };

      craneLibLLvmTools =
        craneLib.overrideToolchain
        (fenix.packages.${system}.complete.withComponents [
          "cargo"
          "llvm-tools"
          "rustc"
        ]);

      # Build *just* the cargo dependencies, so we can reuse
      # all of that work (e.g. via cachix) when running in CI
      cargoArtifacts = craneLib.buildDepsOnly commonArgs;

      # Build the actual crate itself, reusing the dependency
      # artifacts from above.
      my-crate =
        craneLib.buildPackage (commonArgs // {inherit cargoArtifacts;});
    in {
      checks =
        {
          # Build the crate as part of `nix flake check` for convenience
          inherit my-crate;

          # Run clippy (and deny all warnings) on the crate source,
          # again, resuing the dependency artifacts from above.
          #
          # Note that this is done as a separate derivation so that
          # we can block the CI if there are issues here, but not
          # prevent downstream consumers from building our crate by itself.
          my-crate-clippy = craneLib.cargoClippy (commonArgs
            // {
              inherit cargoArtifacts;
              cargoClippyExtraArgs = "--all-targets -- --deny warnings";
            });

          my-crate-doc =
            craneLib.cargoDoc (commonArgs // {inherit cargoArtifacts;});

          # Check formatting
          my-crate-fmt = craneLib.cargoFmt {inherit src;};

          # Audit dependencies
          my-crate-audit = craneLib.cargoAudit {inherit src advisory-db;};

          # Run tests with cargo-nextest
          # Consider setting `doCheck = false` on `my-crate` if you do not want
          # the tests to run twice
          my-crate-nextest = craneLib.cargoNextest (commonArgs
            // {
              inherit cargoArtifacts;
              partitions = 1;
              partitionType = "count";
            });
        }
        // lib.optionalAttrs (system == "x86_64-linux") {
          # NB: cargo-tarpaulin only supports x86_64 systems
          # Check code coverage (note: this will not upload coverage anywhere)
          my-crate-coverage =
            craneLib.cargoTarpaulin (commonArgs // {inherit cargoArtifacts;});
        };

      packages = {
        default = my-crate;
        my-crate-llvm-coverage =
          craneLibLLvmTools.cargoLlvmCov
          (commonArgs // {inherit cargoArtifacts;});
      };

      apps.default = flake-utils.lib.mkApp {drv = my-crate;};

      devShells.default = pkgs.mkShell {
        inputsFrom = builtins.attrValues self.checks.${system};

        # Additional dev-shell environment variables can be set directly
        # MY_CUSTOM_DEVELOPMENT_VAR = "something else";
        RUST_SRC_PATH = "${fenix.packages.${system}.complete.rust-src}/lib/rustlib/src/rust/library";

        # Extra inputs can be added here
        nativeBuildInputs = with pkgs; [cargo rustc vscode pkg-config glib.dev gtk4.dev gtk3.dev atk.dev];

        # shellHook = ''
        #   printf "glib.dev: ${pkgs.glib.dev}\n"
        #   printf "gtk4.dev: ${pkgs.gtk4.dev}\n"
        #   printf "atk.dev: ${pkgs.atk.dev}\n"
        # '';

      };
    });
}
